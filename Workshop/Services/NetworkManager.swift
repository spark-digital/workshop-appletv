//
//  NetworkManager.swift
//  Workshop
//
//  Created by Marcos Jesus Vivar on 28/07/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit

private let _sharedInstance = NetworkManager()

private let discoverManager = DiscoverManager()
private let findManager = FindManager()
private let searchManager = SearchManager()
private let moviesManager = MoviesManager()

open class NetworkManager: NSObject
{
    // MARK: - Instantiation
    
    open class var sharedInstance: NetworkManager
    {
        return _sharedInstance
    }
    
    override init()
    {}
    
    // MARK: - Movies
    
    public func getMoviesNowPlaying(completionHandler completion: @escaping (MDBMoviesResponseModel?, NSError?) -> ())
    {
        moviesManager.getMoviesNowPlaying { (moviesNowPlaying, error) in
            
            if (moviesNowPlaying != nil)
            {
                completion(moviesNowPlaying, nil)
            }
            else
            {
                completion(nil, error)
            }
        }
    }
    
    public func getUpcomingMovies(completionHandler completion: @escaping (MDBMoviesResponseModel?, NSError?) -> ())
    {
        moviesManager.getUpcomingMovies { (upcomingMovies, error) in
            
            if (upcomingMovies != nil)
            {
                completion(upcomingMovies, nil)
            }
            else
            {
                completion(nil, error)
            }
        }
    }
    
    public func getMovieImagesWith(movieID id: String, completionHandler completion: @escaping (MDBImagesResponseModel?, NSError?) -> ())
    {
        moviesManager.getMovieImagesWith(movieID: id,  completionHandler: { (movieImages, error) in
            
            if (movieImages != nil)
            {
                completion(movieImages, nil)
            }
            else
            {
                completion(nil, error)
            }
        })
    }
    
    // MARK: - Search
    
    // MARK: - Discover
    
    // MARK: - Find
}
