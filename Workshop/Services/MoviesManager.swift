//
//  MoviesManager.swift
//  Workshop
//
//  Created by Marcos Jesus Vivar on 7/28/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import Foundation

private let _sharedInstance = MoviesManager()

open class MoviesManager: NSObject
{
    // MARK: - Instantiation
    
    open class var sharedInstance: MoviesManager
    {
        return _sharedInstance
    }
    
    override init()
    {}
    
    // MARK: - Public
    
    public func getMoviesNowPlaying(completionHandler completion: @escaping (MDBMoviesResponseModel?, NSError?) -> ())
    {
        let moviesURLString: String = "\(TMDB_BASE_URL)movie/now_playing?api_key=\(TMDB_API_KEY)&language=en-US&page=1"
        
        BasicNetworkService.performRequest(urlString: moviesURLString) { (json, error) in
            
            if (json != nil)
            {
                let moviesNowPlaying = MDBMoviesResponseModel.init(json: json!)
                
                completion(moviesNowPlaying, nil)
            }
            else
            {
                completion(nil, error)
            }
        }
    }
    
    public func getMoviesTopRated(completionHandler completion: @escaping (MDBMoviesResponseModel?, NSError?) -> ())
    {
        let moviesURLString: String = "\(TMDB_BASE_URL)movie/top_rated?api_key=\(TMDB_API_KEY)&language=en-US&page=1"
        
        BasicNetworkService.performRequest(urlString: moviesURLString) { (json, error) in
            
            if (json != nil)
            {
                let moviesNowPlaying = MDBMoviesResponseModel.init(json: json!)
                
                completion(moviesNowPlaying, nil)
            }
            else
            {
                completion(nil, error)
            }
        }
    }
    
    public func getMovieImagesWith(movieID id: String,completionHandler completion: @escaping (MDBImagesResponseModel?, NSError?) -> ())
    {
        let movieImagesURLString: String = "\(TMDB_BASE_URL)movie/\(id)/images?api_key=\(TMDB_API_KEY)&language=en-US"
        
        BasicNetworkService.performRequest(urlString: movieImagesURLString) { (json, error) in
            
            if (json != nil)
            {
                let movieImages = MDBImagesResponseModel.init(json: json!)
                
                completion(movieImages, nil)
            }
            else
            {
                completion(nil, error)
            }
        }
    }
}
