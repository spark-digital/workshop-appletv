//
//  TopRatedMoviesViewController.swift
//  Workshop
//
//  Created by Marcos Jesús Vivar on 7/28/17.
//  Copyright © 2017 Spark Digital. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class TopRatedMoviesViewController: UICollectionViewController
{
    var moviesDataSource: MoviesDataSource!
    var moviesPlayingInTheaters: MDBMoviesResponseModel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        moviesDataSource = MoviesDataSource()
        
        self.collectionView!.register(MovieCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        getMoviesNowPlaying()
    }
    
    func getMoviesNowPlaying()
    {
        moviesDataSource.getMoviesNowPlaying(completionHandler: { (moviesNowPlaying, error) in
            
            if (moviesNowPlaying != nil)
            {
                self.moviesPlayingInTheaters = moviesNowPlaying
                self.updateUserInterface()
                print("Success!")
            }
            else
            {
                print("Error: \(String(describing: error?.localizedDescription))")
            }
        })
    }
    
    func updateUserInterface()
    {
        self.collectionView?.reloadData()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if ((moviesPlayingInTheaters != nil) && (moviesPlayingInTheaters.results.count > 0))
        {
            return moviesPlayingInTheaters.results.count
        }
        
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        let movie = moviesPlayingInTheaters.results[indexPath.row]
        
        let moviePoster: UIImageView = UIImageView.init(frame: CGRect.init(x: 10.0, y: 10.0, width: 270.0, height: 400.0))
        
        let posterURLString = "\(TMDB_IMAGE_BASE_URL)\(movie.moviePosterPath)"
        let posterURL = URL.init(string: posterURLString)!
        
        moviePoster.af_setImage(withURL: posterURL)
        
        cell.addSubview(moviePoster)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, canFocusItemAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldUpdateFocusIn context: UICollectionViewFocusUpdateContext) -> Bool
    {
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        if let previousIndexPath = context.previouslyFocusedIndexPath, let cell = collectionView.cellForItem(at: previousIndexPath)
        {
            UIView.animate(withDuration: 0.25, animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: { (Bool) in
            })
        }
        
        if let indexPath = context.nextFocusedIndexPath, let cell = collectionView.cellForItem(at: indexPath)
        {
            UIView.animate(withDuration: 0.25, animations: {
                cell.transform = CGAffineTransform(scaleX: 1.15, y: 1.15)
            }, completion: { (Bool) in
            })
            
            collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
        }
    }
}
