//
//  SongsDataSource.swift
//  Workshop
//
//  Created by Marcos Jesús Vivar on 7/28/17.
//  Copyright © 2017 Spark Digital. All rights reserved.
//

import UIKit

open class SongsDataSource: NSObject
{
    override init()
    {
        super.init()
    }
    
    public func getMoviesNowPlaying(completionHandler completion: @escaping (MDBMoviesResponseModel?, NSError?) -> ())
    {
        NetworkManager.sharedInstance.getMoviesNowPlaying(completionHandler: { (moviesNowPlaying, error) in
            
            completion(moviesNowPlaying, error)
        })
    }
    
    public func getUpcomingMovies(completionHandler completion: @escaping (MDBMoviesResponseModel?, NSError?) -> ())
    {
        NetworkManager.sharedInstance.getUpcomingMovies(completionHandler: { (upcomingMovies, error) in
            
            completion(upcomingMovies, error)
        })
    }
    
    public func getMovieImagesFor(movieID id: String, completionHandler completion: @escaping (MDBImagesResponseModel?, NSError?) -> ())
    {
        NetworkManager.sharedInstance.getMovieImagesWith(movieID: id, completionHandler:  { (movieImages, error) in
            
            completion(movieImages, error)
        })
    }
}
