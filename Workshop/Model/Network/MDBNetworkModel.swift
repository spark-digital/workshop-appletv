//
//  MDBNetworkModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/18/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBNetworkModel: NSObject
{
    public var networkID: Int = 0
    public var networkName: String = ""
    
    init(json: JSON)
    {
        self.networkID = json[attributes.id.rawValue].intValue
        self.networkName = json[attributes.name.rawValue].stringValue
    }
}
