//
//  MDBGenreModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/19/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBGenreModel: NSObject
{
    public var genreID: Int = 0
    public var genreName: String = ""
    
    init(json: JSON)
    {
        self.genreID = json[attributes.id.rawValue].intValue
        self.genreName = json[attributes.name.rawValue].stringValue
    }
}
