//
//  MDBCompanyModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/18/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBCompanyModel: NSObject
{
    public var companyID: Int = 0
    public var companyName: String = ""
    public var companyDescription: String = ""
    public var companyHeadquarters: String = ""
    public var companyHomepage: String = ""
    public var companyLogo: String = ""
    public var companyParentCompany: String = ""
    
    init(json: JSON)
    {
        self.companyID = json[attributes.id.rawValue].intValue
        self.companyName = json[attributes.name.rawValue].stringValue
        self.companyDescription = json[attributes.description.rawValue].stringValue
        self.companyHeadquarters = json[attributes.headquarters.rawValue].stringValue
        self.companyHomepage = json[attributes.homepage.rawValue].stringValue
        self.companyLogo = json[attributes.logoPath.rawValue].stringValue
        self.companyParentCompany = json[attributes.parentCompany.rawValue].stringValue
    }
}
