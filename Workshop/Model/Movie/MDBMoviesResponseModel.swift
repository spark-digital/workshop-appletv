//
//  MDBMoviesResponseModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 6/23/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBMoviesResponseModel: NSObject
{
    public var results: [MDBMovieModel] = []
    public var minimumDate: String? = ""
    public var maximumDate: String? = ""
    public var page: Int = 0
    public var totalPages: Int = 0
    public var totalResults: Int = 0
    
    init(json: JSON)
    {
        self.page = json[attributes.page.rawValue].intValue
        self.totalPages = json[attributes.totalPages.rawValue].intValue
        self.totalResults = json[attributes.totalResults.rawValue].intValue
        self.minimumDate = json[attributes.dates.rawValue][attributes.minimum.rawValue].stringValue
        self.maximumDate = json[attributes.dates.rawValue][attributes.maximum.rawValue].stringValue
        
        let movieObjects = json[attributes.results.rawValue].array
        for object in movieObjects!
        {
            let movie = MDBMovieModel.init(json: object)
            
            self.results.append(movie)
        }
    }
}
