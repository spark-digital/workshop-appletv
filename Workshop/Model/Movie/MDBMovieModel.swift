//
//  MDBMovieModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/19/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBMovieModel: NSObject
{
    public var movieID: String = ""
    public var movieName: String = ""
    public var movieAdult: Bool = false
    public var movieBackdropPath: String = ""
    public var movieBelongsToCollection: MDBCollectionModel
    public var movieBudget: Int = 0
    public var movieGenres: [MDBGenreModel] = []
    public var movieHomepage: String = ""
    public var movieIMDBID: String = ""
    public var movieOriginalLanguage: String = ""
    public var movieOriginalTitle: String = ""
    public var movieOverview: String = ""
    public var moviePopularity: Double = 0.0
    public var moviePosterPath: String = ""
    public var movieProductionCompanies: [MDBCompanyModel] = []
    public var movieProductionCountries: [MDBCountryModel] = []
    public var movieReleaseDate: String = ""
    public var movieRevenue: Int = 0
    public var movieRuntime: Int = 0
    public var movieSpokenLanguages: [MDBLanguageModel] = []
    public var movieStatus: String = ""
    public var movieTagline: String = ""
    public var movieTitle: String = ""
    public var movieVideo: Bool = false
    public var movieVoteAverage: Double = 0.0
    public var movieVoteCount: Int = 0
    
    init(json: JSON)
    {
        self.movieID = json[attributes.id.rawValue].stringValue 
        self.movieName = json[attributes.name.rawValue].stringValue
        self.movieAdult = json[attributes.adult.rawValue].boolValue
        self.movieBackdropPath = json[attributes.backdropPath.rawValue].stringValue
        self.movieBelongsToCollection = MDBCollectionModel.init(json: json[attributes.belongsToCollection.rawValue])
        self.movieBudget = json[attributes.budget.rawValue].intValue
        
        let genresObjects = json[attributes.genres.rawValue].array
        if (genresObjects != nil)
        {
            for object in genresObjects!
            {
                let genre = MDBGenreModel.init(json: object)
                
                self.movieGenres.append(genre)
            }
        }
        
        self.movieHomepage = json[attributes.homepage.rawValue].stringValue
        self.movieIMDBID = json[attributes.imdbID.rawValue].stringValue
        self.movieOriginalLanguage = json[attributes.originalLanguage.rawValue].stringValue
        self.movieOriginalTitle = json[attributes.originalTitle.rawValue].stringValue
        self.movieOverview = json[attributes.overview.rawValue].stringValue
        self.moviePopularity = json[attributes.popularity.rawValue].doubleValue
        self.moviePosterPath = json[attributes.posterPath.rawValue].stringValue
        
        let productionCompanies = json[attributes.productionCompanies.rawValue].array
        if (productionCompanies != nil)
        {
            for object in productionCompanies!
            {
                let company = MDBCompanyModel.init(json: object)
                
                self.movieProductionCompanies.append(company)
            }
        }

        let productionCountries = json[attributes.productionCountries.rawValue].array
        if (productionCountries != nil)
        {
            for object in productionCountries!
            {
                let country = MDBCountryModel.init(json: object)
                
                self.movieProductionCountries.append(country)
            }
        }
        
        self.movieReleaseDate = json[attributes.releaseDate.rawValue].stringValue
        self.movieRevenue = json[attributes.revenue.rawValue].intValue
        self.movieRuntime = json[attributes.runtime.rawValue].intValue
        
        let spokenLanguages = json[attributes.spokenLanguages.rawValue].array
        if (spokenLanguages != nil)
        {
            for object in spokenLanguages!
            {
                let language = MDBLanguageModel.init(json: object)
                
                self.movieSpokenLanguages.append(language)
            }
        }
        
        self.movieStatus = json[attributes.status.rawValue].stringValue
        self.movieTagline = json[attributes.tagline.rawValue].stringValue
        self.movieTitle = json[attributes.title.rawValue].stringValue
        self.movieVideo = json[attributes.video.rawValue].boolValue
        self.movieVoteAverage = json[attributes.voteAverage.rawValue].doubleValue
        self.movieVoteCount = json[attributes.voteCount.rawValue].intValue
    }
}
