//
//  MDBImagesResponseModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesús Vivar on 7/7/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBImagesResponseModel: NSObject
{
    public var imageID: Int = 0
    
    public var backdrops: [MDBImageModel] = []
    public var posters: [MDBImageModel] = []
    
    init(json: JSON)
    {
        self.imageID = json[attributes.id.rawValue].intValue
        
        let backdropsObjects = json[attributes.backdrops.rawValue].array
        if (backdropsObjects != nil)
        {
            for object in backdropsObjects!
            {
                let image = MDBImageModel.init(json: object)
                
                self.backdrops.append(image)
            }
        }
        
        let postersObjects = json[attributes.posters.rawValue].array
        if (postersObjects != nil)
        {
            for object in postersObjects!
            {
                let image = MDBImageModel.init(json: object)
                
                self.posters.append(image)
            }
        }
    }
}
