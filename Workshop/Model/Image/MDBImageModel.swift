//
//  MDBImageModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesús Vivar on 7/7/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBImageModel: NSObject
{
    public var imageAspectRatio: Double = 0.0
    public var imagePath: String = ""
    public var imageWidth: Int = 0
    public var imageHeight: Int = 0
    public var imageISO6391: String = ""
    public var imageVoteAverage: Int = 0
    public var imageVoteCount: Int = 0
    
    init(json: JSON)
    {
        self.imageAspectRatio = json[attributes.aspectRatio.rawValue].doubleValue
        self.imagePath = json[attributes.filePath.rawValue].stringValue
        self.imageWidth = json[attributes.width.rawValue].intValue
        self.imageHeight = json[attributes.height.rawValue].intValue
        self.imageISO6391 = json[attributes.iso6391.rawValue].stringValue
        self.imageVoteAverage = json[attributes.voteAverage.rawValue].intValue
        self.imageVoteCount = json[attributes.voteCount.rawValue].intValue
    }
}
