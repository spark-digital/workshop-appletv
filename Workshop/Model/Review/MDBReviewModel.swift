//
//  MDBReviewModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/18/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBReviewModel: NSObject
{
    public var reviewID: String = ""
    public var reviewAuthor: String = ""
    public var reviewContent: String = ""
    public var reviewISO6391: String = ""
    public var reviewMediaID: Int = 0
    public var reviewMediaTitle: String = ""
    public var reviewType: String = ""
    public var reviewURL: String = ""
    
    init(json: JSON)
    {
        self.reviewID = json[attributes.id.rawValue].stringValue
        self.reviewAuthor = json[attributes.name.rawValue].stringValue
        self.reviewContent = json[attributes.description.rawValue].stringValue
        self.reviewISO6391 = json[attributes.headquarters.rawValue].stringValue
        self.reviewMediaID = json[attributes.homepage.rawValue].intValue
        self.reviewMediaTitle = json[attributes.logoPath.rawValue].stringValue
        self.reviewType = json[attributes.parentCompany.rawValue].stringValue
        self.reviewURL = json[attributes.parentCompany.rawValue].stringValue
    }
}
