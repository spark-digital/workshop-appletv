//
//  MDBCountryModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/19/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBCountryModel: NSObject
{
    public var countryISO31661: String = ""
    public var countryName: String = ""
    
    init(json: JSON)
    {
        self.countryISO31661 = json[attributes.iso31661.rawValue].stringValue
        self.countryName = json[attributes.name.rawValue].stringValue
    }
}
