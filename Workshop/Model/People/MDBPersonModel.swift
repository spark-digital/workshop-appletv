//
//  MDBPersonModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/18/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBPersonModel: NSObject
{
    public var personID: Int = 0
    public var personName: String = ""
    public var personAKA: String = ""
    public var personBiography: String = ""
    public var personBirthday: String = ""
    public var personDeathday: String = ""
    public var personAdult: Bool = false
    public var personGender: Int = 0
    public var personHomepage: String = ""
    public var personIMDBID: String = ""
    public var personPlaceOfBirth: String = ""
    public var personPopularity: Double = 0.0
    public var personProfilePath: String = ""
    
    init(json: JSON)
    {
        self.personID = json[attributes.id.rawValue].intValue
        self.personName = json[attributes.name.rawValue].stringValue
        self.personAKA = json[attributes.alsoKnownAs.rawValue].stringValue
        self.personBiography = json[attributes.biography.rawValue].stringValue
        self.personBirthday = json[attributes.birthday.rawValue].stringValue
        self.personDeathday = json[attributes.deathday.rawValue].stringValue
        self.personAdult = json[attributes.adult.rawValue].boolValue
        self.personGender = json[attributes.gender.rawValue].intValue
        self.personHomepage = json[attributes.homepage.rawValue].stringValue
        self.personIMDBID = json[attributes.imdbID.rawValue].stringValue
        self.personPlaceOfBirth = json[attributes.placeOfBirth.rawValue].stringValue
        self.personPopularity = json[attributes.popularity.rawValue].doubleValue
        self.personProfilePath = json[attributes.profilePath.rawValue].stringValue
    }
}
