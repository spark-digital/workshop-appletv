//
//  MDBLanguageModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/19/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBLanguageModel: NSObject
{
    public var languageISO6391: Int = 0
    public var languageName: String = ""
    
    init(json: JSON)
    {
        self.languageISO6391 = json[attributes.iso6391.rawValue].intValue
        self.languageName = json[attributes.name.rawValue].stringValue
    }
}
