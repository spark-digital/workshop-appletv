//
//  MDBCollectionPartModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/19/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBCollectionPartModel: NSObject
{
    public var partID: Int = 0
    public var partTitle: String = ""
    public var partOriginalTitle: String = ""
    public var partOriginalLanguage: String = ""
    public var partAdult: Bool = false
    public var partOverview: String = ""
    public var partBackdropPath: String = ""
    public var partGenreIDs: [Int] = []
    public var partReleaseDate: String = ""
    public var partPosterPath: String = ""
    public var partPopularity: String = ""
    public var partVideo: String = ""
    public var partVoteAverage: Double = 0.0
    public var partVoteCount: Int = 0
    
    init(json: JSON)
    {
        self.partID = json[attributes.id.rawValue].intValue
        self.partTitle = json[attributes.title.rawValue].stringValue
        self.partOriginalTitle = json[attributes.originalTitle.rawValue].stringValue
        self.partOriginalLanguage = json[attributes.originalLanguage.rawValue].stringValue
        self.partAdult = json[attributes.adult.rawValue].boolValue
        self.partOverview = json[attributes.overview.rawValue].stringValue
        self.partBackdropPath = json[attributes.backdropPath.rawValue].stringValue
        
        let genresObjects = json[attributes.genreIDs.rawValue].array
        for object in genresObjects!
        {
            self.partGenreIDs.append(object.intValue)
        }
        
        self.partReleaseDate = json[attributes.releaseDate.rawValue].stringValue
        self.partPosterPath = json[attributes.posterPath.rawValue].stringValue
        self.partPopularity = json[attributes.popularity.rawValue].stringValue
        self.partVideo = json[attributes.video.rawValue].stringValue
        self.partVoteAverage = json[attributes.voteAverage.rawValue].doubleValue
        self.partVoteCount = json[attributes.voteCount.rawValue].intValue
    }
}
