//
//  MDBCollectionModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/19/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBCollectionModel: NSObject
{
    public var collectionID: Int = 0
    public var collectionName: String = ""
    public var collectionOverview: String = ""
    public var collectionPosterPath: String = ""
    public var collectionBackdropPath: String = ""
    public var collectionParts: [MDBCollectionPartModel] = []
    
    init(json: JSON)
    {
        self.collectionID = json[attributes.id.rawValue].intValue
        self.collectionName = json[attributes.name.rawValue].stringValue
        self.collectionOverview = json[attributes.overview.rawValue].stringValue
        self.collectionPosterPath = json[attributes.posterPath.rawValue].stringValue
        self.collectionBackdropPath = json[attributes.backdropPath.rawValue].stringValue
        
        let partsObjects = json[attributes.parts.rawValue].array
        
        if (partsObjects != nil)
        {
            for object in partsObjects!
            {
                let part = MDBCollectionPartModel.init(json: object)
                
                self.collectionParts.append(part)
            }
        }
    }
}
