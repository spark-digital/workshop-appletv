//
//  MDBTVShowModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/18/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBTVShowModel: NSObject
{
    public var showID: String = ""
    public var showName: String = ""
    public var showBackdropPath: String = ""
    public var showCreatedBy: String = ""
    public var showEpisodeRunTime: Int = 0
    public var showFirstAirDate: String = ""
    public var showGenres: [MDBGenreModel] = []
    public var showHomepage: String = ""
    public var showInProduction: Bool = false
    public var showLanguages: [String] = []
    public var showLastAirDate: String = ""
    public var showNetworks: [MDBNetworkModel] = []
    public var showNumberOfEpisodes: Int = 0
    public var showNumberOfSeasons: Int = 0
    public var showOriginCountry: [String] = []
    public var showOriginalLanguage: String = ""
    public var showOriginalName: String = ""
    public var showOverview: String = ""
    public var showPopularity: String = ""
    public var showPosterPath: String = ""
    public var showProductionCompanies: [MDBCompanyModel] = []
    public var showSeasons: [MDBSeasonModel] = []
    public var showStatus: String = ""
    public var showType: String = ""
    public var showVoteAverage: Double = 0.0
    public var showVoteCount: Int = 0
    
    init(json: JSON)
    {
        self.showID = json[attributes.id.rawValue].stringValue
        self.showName = json[attributes.name.rawValue].stringValue
        self.showBackdropPath = json[attributes.backdropPath.rawValue].stringValue
        self.showCreatedBy = json[attributes.createdBy.rawValue].stringValue
        self.showEpisodeRunTime = json[attributes.episodeRunTime.rawValue].intValue
        self.showFirstAirDate = json[attributes.firstAirDate.rawValue].stringValue
        
        let genresObjects = json[attributes.genres.rawValue].array
        for object in genresObjects!
        {
            let genre = MDBGenreModel.init(json: object)

            self.showGenres.append(genre)
        }
        
        self.showHomepage = json[attributes.homepage.rawValue].stringValue
        self.showInProduction = json[attributes.inProduction.rawValue].boolValue
        
        let languagesObjects = json[attributes.languages.rawValue].array
        for language in languagesObjects!
        {
            self.showLanguages.append(language.stringValue)
        }
        
        self.showLastAirDate = json[attributes.lastAirDate.rawValue].stringValue
        
        let networksObjects = json[attributes.networks.rawValue].array
        for object in networksObjects!
        {
            let network = MDBNetworkModel.init(json: object)
            
            self.showNetworks.append(network)
        }
        
        self.showNumberOfEpisodes = json[attributes.numberOfEpisodes.rawValue].intValue
        self.showNumberOfSeasons = json[attributes.numberOfSeasons.rawValue].intValue
        
        let countries = json[attributes.originCountry.rawValue].array
        for country in countries!
        {
            self.showOriginCountry.append(country.stringValue)
        }
        
        self.showOriginalLanguage = json[attributes.originalLanguage.rawValue].stringValue
        self.showOriginalName = json[attributes.originalName.rawValue].stringValue
        self.showOverview = json[attributes.overview.rawValue].stringValue
        self.showPopularity = json[attributes.popularity.rawValue].stringValue
        self.showPosterPath = json[attributes.posterPath.rawValue].stringValue
        
        let productionCompanies = json[attributes.productionCompanies.rawValue].array
        for object in productionCompanies!
        {
            let company = MDBCompanyModel.init(json: object)
            
            self.showProductionCompanies.append(company)
        }
        
        let seasons = json[attributes.seasons.rawValue].array
        for object in seasons!
        {
            let season = MDBSeasonModel.init(json: object)
            
            self.showSeasons.append(season)
        }
        
        self.showStatus = json[attributes.status.rawValue].stringValue
        self.showType = json[attributes.type.rawValue].stringValue
        self.showVoteAverage = json[attributes.voteAverage.rawValue].doubleValue
        self.showVoteCount = json[attributes.voteCount.rawValue].intValue
    }
}
