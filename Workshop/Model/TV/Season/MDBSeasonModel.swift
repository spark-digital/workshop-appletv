//
//  MDBSeasonModel.swift
//  Cinemasaurus
//
//  Created by Marcos Jesus Vivar on 4/18/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit
import SwiftyJSON

open class MDBSeasonModel: NSObject
{
    public var seasonID: Int = 0
    public var seasonEpisodeCount: Int = 0
    public var seasonNumber: Int = 0
    public var seasonAirDate: String = ""
    public var seasonPosterPath: String = ""
    
    init(json: JSON)
    {
        self.seasonID = json[attributes.id.rawValue].intValue
        self.seasonEpisodeCount = json[attributes.episodeCount.rawValue].intValue
        self.seasonNumber = json[attributes.seasonNumber.rawValue].intValue
        self.seasonAirDate = json[attributes.airDate.rawValue].stringValue
        self.seasonPosterPath = json[attributes.posterPath.rawValue].stringValue
    }
}
