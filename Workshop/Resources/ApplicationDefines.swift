//
//  ApplicationDefines.swift
//  Workshop
//
//  Created by Marcos Jesus Vivar on 7/28/17.
//  Copyright © 2017 Marcos Jesus Vivar. All rights reserved.
//

import UIKit

public enum attributes: String
{
    case adult = "adult"
    case airDate = "air_date"
    case alsoKnownAs = "also_known_as"
    case aspectRatio = "aspect_ratio"
    case author = "author"
    case backdrops = "backdrops"
    case backdropPath = "backdrop_path"
    case belongsToCollection = "belongs_to_collection"
    case biography = "biography"
    case birthday = "birthday"
    case budget = "budget"
    case character = "character"
    case content = "content"
    case createdBy = "created_by"
    case creditID = "credit_id"
    case crew = "crew"
    case dates = "dates"
    case deathday = "deathday"
    case department = "department"
    case description = "description"
    case episodeCount = "episode_count"
    case episodeNumber = "episode_number"
    case episodeRunTime = "episode_run_time"
    case filePath = "file_path"
    case firstAirDate = "first_air_date"
    case gender = "gender"
    case genreIDs = "genre_ids"
    case genres = "genres"
    case guestStars = "guest_stars"
    case headquarters = "headquarters"
    case height = "height"
    case homepage = "homepage"
    case id = "id"
    case imdbID = "imdb_id"
    case inProduction = "in_production"
    case iso31661 = "iso_3166_1"
    case iso6391 = "iso_639_1"
    case job = "job"
    case knownFor = "known_for"
    case languages = "languages"
    case lastAirDate = "last_air_date"
    case logoPath = "logo_path"
    case maximum = "maximum"
    case mediaID = "media_id"
    case mediaTitle = "media_title"
    case mediaType = "media_type"
    case minimum = "minimum"
    case name = "name"
    case networks = "networks"
    case numberOfEpisodes = "number_of_episodes"
    case numberOfSeasons = "number_of_seasons"
    case order = "order"
    case originalName = "original_name"
    case originalLanguage = "original_language"
    case originalTitle = "original_title"
    case originCountry = "origin_country"
    case overview = "overview"
    case page = "page"
    case parentCompany = "parent_company"
    case parts = "parts"
    case placeOfBirth = "place_of_birth"
    case popularity = "popularity"
    case posterPath = "poster_path"
    case posters = "posters"
    case productionCode = "production_code"
    case productionCompanies = "production_companies"
    case productionCountries = "production_countries"
    case profilePath = "profile_path"
    case releaseDate = "release_date"
    case results = "results"
    case revenue = "revenue"
    case runtime = "runtime"
    case seasonNumber = "season_number"
    case seasons = "seasons"
    case spokenLanguages = "spoken_languages"
    case status = "status"
    case stillPath = "still_path"
    case tagline = "tagline"
    case title = "title"
    case totalPages = "total_pages"
    case totalResults = "total_results"
    case type = "type"
    case url = "url"
    case video = "video"
    case voteAverage = "vote_average"
    case voteCount = "vote_count"
    case width = "width"
}

/*
*/

public enum endpoints: String
{
    case DISCOVER = "discover"
    case FIND = "find"
    case SEARCH = "search"
}

let newMovieFocusedNotification = "NewMovieFocusedNotification"

let maxLengthForVisibleTitle = 40
